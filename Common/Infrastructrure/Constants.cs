﻿using System.IO;

namespace Common.Infrastructrure
{
    public static class Constants
    {
        public const string FileDoesntExist = "File doesn't exist";
        public const string Eof = "<EOF>";
        public const string Urg = "<Urg>";
        public const string ConnectionWasClosed = "Connection was closed";

        public const int UdpClientPortNumber = 50061;
        public const int TcpClientPortNumber = 50060;

        public const int ServerPortNumber = 50060;

        public const int MaxSocketsCount = 10;

        public static readonly string ClientFolderPath = Path.Combine("D:", "SPOLKS_LAB_1_TEST", "ClientFolder");
        public static readonly string ServerFolderPath = Path.Combine("D:", "SPOLKS_LAB_1_TEST", "ServerFolder");


        public static class Method
        {
            public const string Echo = "ECHO";
            public const string Time = "Time";
            public const string Close = "Close";
            public const string Download = "Download";
        }
    }
}