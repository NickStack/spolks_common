﻿using System;
using System.Net.Sockets;

namespace Common.Infrastructrure
{
    public static class SocketConfigurator
    {
        public static void ConfigureSocket(out Socket socket, AddressFamily addressFamily)
        {
            socket = new Socket(addressFamily, SocketType.Stream, ProtocolType.Tcp);
            const int size = sizeof(uint);
            const int on = 1;
            const uint keepAliveInterval = 10000; //Send a packet once every 10 seconds.
            const uint retryInterval = 1000; //If no response, resend every second.
            var inArray = new byte[size * 3];
            Array.Copy(BitConverter.GetBytes(@on), 0, inArray, 0, size);
            Array.Copy(BitConverter.GetBytes(keepAliveInterval), 0, inArray, size, size);
            Array.Copy(BitConverter.GetBytes(retryInterval), 0, inArray, size * 2, size);
            socket.IOControl(IOControlCode.KeepAliveValues, inArray, null);
        }
    }
}