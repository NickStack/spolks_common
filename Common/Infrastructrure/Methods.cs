﻿namespace Common.Infrastructrure
{
    public enum Methods
    {
        Echo,
        Time,
        Close,
        Download,
        PackageReceived
    }
}